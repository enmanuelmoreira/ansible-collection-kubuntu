echo "Ejecutando prerequisitos..."
echo "Actualizando repositorios..."
sudo apt-get update
echo "Instalando pip"
sudo apt-get install -y python3-pip
echo "Instalando Ansible..." 
sudo pip3 install ansible
echo "Configurando sistema..."
ansible-playbook main.yml
